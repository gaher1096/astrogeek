# AstroGeek

_Proyecto sitio web / tienda virtual [AstroGeek](https://astrogeektienda.co/)_

## Construido con 🛠️

* [Laravel 8.x](https://laravel.com/) - El framework web basado en PHP usado.
* [Bootstrap 4.5](https://getbootstrap.com/) - El front-end open source toolkit usado.
* [Fontawesome 4.7.0](https://fontawesome.com/v4.7.0/) - El The iconic font y CSS toolkit usado.

## Autores ✒️

* **Gabriel Ordoñez** - *Trabajo Inicial* - [gaher1096](https://gitlab.com/gaher1096)

    También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/gaher1096/astrogeek/-/graphs/Develop) quíenes han participado en este proyecto. 

## Expresiones de Gratitud 🎁

* **Mauricio** - *Canal de Youtube - Con información sobre laravel* - [Mauricio Developer](https://www.youtube.com/channel/UCdBs2T2oThQwBZV40PO6ZRA)
* **freepik** - *Proveedor de Recursos gráficos usado en el proyecto.* - [freepik](https://www.freepik.es/)
