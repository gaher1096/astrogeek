<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConnectController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Auth Routes */
Route::get('login', [ConnectController::class, 'getLogin'])->name('view.login')->middleware('guest');
Route::post('login', [ConnectController::class, 'postLogin'])->name('user.login')->middleware('guest');
Route::get('logout', [ConnectController::class, 'getLogout'])->name('user.logout');
Route::get('register', [ConnectController::class, 'getRegister'])->name('view.register')->middleware('guest');
Route::post('register', [ConnectController::class, 'storeRegister'])->name('user.register')->middleware('guest');

Route::middleware(['auth', 'admin'])->prefix('/admin')->group(function () {
    Route::get('/', [DashboardController::class, 'getDashboard'])->name('admin.dashboard');
    Route::get('/users', [UserController::class, 'getUsers'])->name('admin.users');
    Route::resource('/categories', CategoryController::class);
});
