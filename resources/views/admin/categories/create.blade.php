@extends('admin.master')

@section('title')
    Crear Categoria
@endsection

@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{ url('/admin/categories') }}">Categorías</a></li>
@endsection


@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-persiang"><i class="fa fa-tag" aria-hidden="true"></i> Crear Categoria</h6>
        </div>
        <div class="card-body">
            @if (Session::has('message'))
                <div class="container">
                    <div class="alert alert-{{Session::get('typealert')}} alert-dismissible fade show" role="alert">
                        {{Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <form class="form-group" method="POST" action="{{url('/admin/categories')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name"><i class="fa fa-tag" aria-hidden="true"></i> Nombre Categoria*</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="nombre de la categoria." value="{{old('name')}}">
                    @error('name')
                        <small id="name" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="description"><i class="fa fa-tag" aria-hidden="true"></i> Descripción Categoria</label>
                    <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="Descripción de la categoria.">{{old('description')}}</textarea>
                    @error('description')
                        <small id="description" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="icon"><i class="fa fa-tag" aria-hidden="true"></i> Ícono</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input @error('icon') is-invalid @enderror" name="icon" id="icon" accept=".png, .svg" value="{{old('icon')}}">
                        <label class="custom-file-label" for="icon"><i class="fa fa-upload" aria-hidden="true"></i> Elija el archivo.</label>
                    </div>
                    @error('icon')
                        <small id="icon" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="attribution"><i class="fa fa-tag" aria-hidden="true"></i> Atribución creador del ícono</label>
                    <textarea type="text" class="form-control @error('attribution') is-invalid @enderror" name="attribution" id="attribution" placeholder="Atribución creador del ícono.">{{old('attribution')}}</textarea>
                    @error('attribution')
                        <small id="attribution" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-persiang btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Crear</button>
            </form>
        </div>
    </div>
@endsection
