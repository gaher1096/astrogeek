@extends('admin.master')

@section('title')
    Categorías
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-flex bd-highlight">
                <div class="flex-grow-1 bd-highlight">
                    <h6 class="mt-2 font-weight-bold text-persiang">
                        <i class="fa fa-tags" aria-hidden="true"></i> Listado de Categorías
                    </h6>
                </div>
                <div class="bd-highlight">
                    <a class="btn btn-outline-success btn-sm" href="{{ url('/admin/categories/create') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if (Session::has('message'))
                <div class="container">
                    <div class="alert alert-{{Session::get('typealert')}} alert-dismissible fade show" role="alert">
                        {{Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">ID</th>
                            <th class="text-center" scope="col">Nombre</th>
                            <th class="text-center" scope="col">Slug</th>
                            <th class="text-center" scope="col">Descripción</th>
                            <th class="text-center" scope="col">Condición</th>
                            <th class="text-center" scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($categories->items()))
                            <tr>
                                <th class="text-center" colspan="6">
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    No se ha podido encontrar ninguna categoría.
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                </th>
                            </tr>
                        @else
                            @foreach ($categories as $category)
                            <tr>
                                <th class="text-center" scope="row" nowrap>{{ $category->id }}</th>
                                <td nowrap>{{ $category->name }}</td>
                                <td nowrap>{{ $category->slug }}</td>
                                <td>{{ substr($category->description,0,50) }}...</td>
                                <td class="text-center" nowrap>
                                    @if ($category->trashed())
                                        <span class="badge badge-warning">Inactiva.</span>
                                    @else
                                        <span class="badge badge-success">Activa.</span>
                                    @endif
                                </td>
                                <td class="text-center" nowrap>
                                    <a class="btn btn-outline-success btn-sm" href="{{ url('/admin/categories/'.$category->id) }}" role="button" data-toggle="tooltip" data-placement="top" title="Visualizar"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-warning btn-sm" href="{{ url('/admin/categories/'.$category->id.'/edit') }}" role="button" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-danger btn-sm" href="{{ url('/admin/categories/'.$category->id.'/destroy') }}" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
