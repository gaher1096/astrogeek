@extends('admin.master')

@section('title')
    {{ $category->name }}
@endsection

@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{ url('/admin/categories') }}">Categorías</a></li>
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-flex bd-highlight">
                <div class="bd-highlight">
                    @if ($category->icon != null)
                        <img src="{{url('/uploaded/'.$category->path.'/'.$category->icon)}}" alt="Logo AstroGeek" height="20">
                    @endif
                </div>
                <div class="bd-highlight">
                    <h6 class="font-weight-bold text-persiang m-1">
                        &nbsp;{{ $category->name }}
                    </h6>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if (Session::has('message'))
                <div class="container">
                    <div class="alert alert-{{Session::get('typealert')}} alert-dismissible fade show" role="alert">
                        {{Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-sm table-borderless">
                    <tr>
                        <th scope="row">ID:</th>
                        <td>{{ $category->id }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Categoría:</th>
                        <td>{{ $category->name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Slug:</th>
                        <td>{{ $category->slug }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Ruta ícono:</th>
                        @if ( $category->icon != null)
                            <td>{{ '/uploaded/'.$category->path.'/'.$category->icon }}</td>
                        @else
                            <td>N.A.</td>
                        @endif

                    </tr>
                    <tr>
                        <th scope="row">Creación:</th>
                        <td>{{ $category->created_at }}</td>
                    </tr>
                    <tr>
                        <th scope="row" width="180px" nowrap>Ultima modificación:</th>
                        <td>{{ $category->updated_at }}</td>
                    </tr>
                    <tr>
                        <th scope="row" colspan="2">Descripción:</th>
                    </tr>
                    <tr>
                        @if ( $category->description != null)
                            <td colspan="2">{{ $category->description}}</td>
                        @else
                            <td>N.A.</td>
                        @endif
                    </tr>
                    @if ( $category->attribution != null)
                        <tr>
                            <th scope="row" colspan="2">Atribución:</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $category->attribution }}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th scope="row" colspan="2">ícono:</th>
                    </tr>
                    <tr>
                        @if ($category->icon != null)
                            <td class="text-center" colspan="2">
                                <div class="card">
                                    <img src="{{url('/uploaded/'.$category->path.'/'.$category->icon)}}" alt="{{$category->name}}" height="200">
                                </div>
                            </td>
                        @else
                            <td class="text-center" colspan="2">No se ha añadido un ícono.</td>
                        @endif
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
