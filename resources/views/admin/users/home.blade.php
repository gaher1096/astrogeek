@extends('admin.master')

@section('title')
    Usuarios
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-persiang"><i class="fa fa-users" aria-hidden="true"></i> Listado de Usuarios</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Rol</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Email</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>
                                @if ($user->role == 1)
                                    <span class="badge badge-success">Admin</span>
                                @else
                                <span class="badge badge-warning">usuario</span>
                                @endif
                            </td>
                            <td>{{ $user->names }}</td>
                            <td>{{ $user->firstSurname.' '.$user->secondSurname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a class="btn btn-outline-success btn-sm" href="{{ url('/admin/user/'.$user->id) }}" role="button" data-toggle="tooltip" data-placement="top" title="Visualizar"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <a class="btn btn-outline-warning btn-sm" href="{{ url('/admin/user/'.$user->id.'/edit') }}" role="button" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a class="btn btn-outline-danger btn-sm" href="{{ url('/admin/user/'.$user->id.'/delete') }}" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
