@extends('connect.master')
@section('title')
    Registrarse
@endsection

@section('body')
    <div class="container">
        <div class="card box-register shadow mt-4 mb-4">
            <div class="card-header">
                <div class="text-center">
                    <a href="{{url('/')}}">
                        <img src="{{url('/static/img/LogoAstrogeek-01.svg')}}" alt="Logo AstroGeek" height="100">
                    </a>
                </div>
            </div>
            <div class="card-body">
                <h4 class="text-center"><i class="fa fa-user-o" aria-hidden="true"></i> Regístrate</h4>
                <p class="text-center mb-2 text-spanisho"><small>*Campos obligatorios</small></p>
                @if (Session::has('message'))
                    <div class="container">
                        <div class="alert alert-{{Session::get('typealert')}} alert-dismissible fade show" role="alert">
                            {{Session::get('message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <form method="POST" action="{{url('/register')}}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="names"><i class="fa fa-user" aria-hidden="true"></i> Nombres*</label>
                            <input type="text" class="form-control @error('names') is-invalid @enderror" name="names" id="names" placeholder="nombres">
                            @error('names')
                                <small id="names" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="firstSurname"><i class="fa fa-user" aria-hidden="true"></i> Primer apellido*</label>
                            <input type="text" class="form-control @error('firstSurname') is-invalid @enderror" name="firstSurname" id="firstSurname" placeholder="Apellido">
                            @error('firstSurname')
                                <small id="firstSurname" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="secondSurname"><i class="fa fa-user" aria-hidden="true"></i> Segundo apellido*</label>
                            <input type="text" class="form-control @error('secondSurname') is-invalid @enderror" name="secondSurname" id="secondSurname" placeholder="Apellido">
                            @error('secondSurname')
                                <small id="secondSurname" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Correo Electrónico*</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="nombre@example.com">
                            @error('email')
                                <small id="email" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password"><i class="fa fa-lock" aria-hidden="true"></i> Contraseña*</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Contraseña">
                            @error('password')
                                <small id="password" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="confirmPassword"><i class="fa fa-lock" aria-hidden="true"></i> Confirma tu contraseña*</label>
                            <input type="password" class="form-control @error('confirmPassword') is-invalid @enderror" name="confirmPassword" id="confirmPassword" placeholder="Contraseña">
                            @error('confirmPassword')
                                <small id="confirmPassword" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" name="terms" id="terms">
                        <label class="form-check-label" for="terms">Acepto los <a href="{{url('/terminos')}}">términos y condiciones</a> de servicio.</label>
                        @error('terms')
                            <small id="terms" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-persiang btn-block">REGÍSTRATE</button>
                </form>
                <p class="text-center mt-4">¿Tienes cuenta? <a class="text-dark" href="{{url('/login')}}"><u>Inicia sesión</u></a></p>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <div class="container bg-blackoliv">
        <div class="text-light text-center p-1">
            <p class="mt-3"><a class="moki text-light" href="{{url('/')}}">ASTROGEEK</a> | Todos los derechos reservados | Dirección: Carrera 6 No. 1 - 09, La Plata - Huila, Colombia. Teléfono: +57 316-7468769.</p>
        </div>
    </div>
@endsection
