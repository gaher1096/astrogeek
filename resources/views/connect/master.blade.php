<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{url('/static/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/static/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('/static/css/site.css')}}">
    <title>@yield('title') · AstroGeek</title>
</head>
<body>
    <header>
        @yield('header')
    </header>
    <div class="body">
        @yield('body')
    </div>
    <footer class="footer">
        @yield('footer')
    </footer>

    <script src="{{url('/static/js/jquery-3.5.1.slim.min.js')}}"></script>
    <script src="{{url('/static/js/popper.min.js')}}"></script>
    <script src="{{url('/static/js/bootstrap.min.js')}}"></script>
</body>
</html>
