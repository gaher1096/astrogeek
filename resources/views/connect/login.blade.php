@extends('connect.master')
@section('title')
    Login
@endsection

@section('body')
    <div class="card box box-login shadow">
        <div class="card-header">
            <div class="text-center">
                <a href="{{url('/')}}">
                    <img src="{{url('/static/img/LogoAstrogeek-01.svg')}}" alt="Logo AstroGeek" height="100">
                </a>
            </div>
        </div>
        <div class="card-body">
            <h4 class="text-center mb-4"><i class="fa fa-user-o" aria-hidden="true"></i> Inicia sesión</h4>
            @if (Session::has('message'))
                <div class="">
                    <div class="alert alert-{{Session::get('typealert')}} alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle  fa-lg" aria-hidden="true"></i>&nbsp
                        {{Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <form method="POST" action="{{url('/login')}}">
                @csrf
                <div class="form-group">
                    <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Correo Electrónico</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="nombre@example.com">
                    @error('email')
                        <small id="email" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password"><i class="fa fa-lock" aria-hidden="true"></i> Contraseña</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Contraseña">
                    @error('password')
                        <small id="password" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="text-center mb-2">
                    <a class="text-dark" href="#">¿Olvidaste tu contraseña?</a>
                </div>
                <button type="submit" class="btn btn-persiang btn-block">INICIAR SESIÓN</button>
            </form>
            <p class="text-center mt-4">¿No tienes cuenta? <a class="text-dark" href="{{url('/register')}}"><u>Regístrate</u></a></p>
        </div>
    </div>
@endsection
