<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class userStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'names' => 'required',
            'firstSurname' => 'required',
            'secondSurname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|same:confirmPassword',
            'confirmPassword' => 'required|min:8|same:password',
            'terms' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'names.required' => 'El campo de nombres es obligatorio.',
            'firstSurname.required' => 'El campo de primer apellido es obligatorio.',
            'secondSurname.required' => 'El campo de segundo apellido es obligatorio.',
            'email.required' => 'El campo de correo electrónico es obligatorio.',
            'email.email' => 'El campo correo Electrónico debe seguir el siguiente formato: usuario@ejemplo.com.',
            'email.unique' => 'El correo electrónico ya se encuentra registrado en la plataforma.',
            'password.required' => 'El campo de contraseña es obligatorio.',
            'password.min' => 'La contraseña debe tener mínimo 8 caracteres.',
            'password.same' => 'Las contraseñas no coinciden.',
            'confirmPassword.required' => 'El campo confirma tu contraseña es obligatorio.',
            'confirmPassword.min' => 'La contraseña debe tener mínimo 8 caracteres.',
            'confirmPassword.same' => 'Las contraseñas no coinciden.',
            'terms.required' => 'No has aceptado nuestros  términos y condiciones de servicio.',
        ];
    }
}
