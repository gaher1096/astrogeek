<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class ConnectController extends Controller
{
    public function getLogin()
    {
        return view('connect.login');
    }
    public function postLogin(UserLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            return redirect('/login')->with('message', 'E-mail o Contraseña incorrecta.')->with('typealert', 'danger');
        }
    }
    public function getLogout()
    {
        Auth::logout();
        return redirect('/');
    }
    public function getRegister()
    {
        return view('connect.register')->with('message', '¡Se ha registrado correctamente!')->with('typealert', 'success');;
    }
    public function storeRegister(UserStoreRequest $request)
    {
        $user = new User();
        $user->names = ucwords(strtolower($request->names));
        $user->firstSurname = ucwords(strtolower($request->firstSurname));
        $user->secondSurname = ucwords(strtolower($request->secondSurname));
        $user->email = strtolower($request->email);
        $user->password = bcrypt($request->password);
        $user->terms = $request->terms;
        if ($user->save()) {
            return redirect('/login')->with('message', '¡Se ha registrado correctamente!')->with('typealert', 'success');
        } else {
            return redirect('/register')->with('message', 'No se ha podido realizar el registro, contacte al administrador del sitio.')->with('typealert', 'danger');
        }
    }
}
