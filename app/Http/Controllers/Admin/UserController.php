<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUsers()
    {
        $users = User::select(['id', 'role', 'names', 'firstSurname', 'secondSurname', 'email'])->orderBy('id', 'Asc')->paginate(5);
        return view('admin.users.home')->with('users', $users);
    }
}
