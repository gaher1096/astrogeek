<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\CategoryStoreRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::withTrashed()->orderBy('name', 'Asc')->paginate(5);
        return view('admin.categories.home')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $fileName = null;
        $path = null;
        if ($request->hasFile('icon')) {
            $path = date('y-m-d'); // Directorio basado en la Fecha
            $fileExt = trim($request->file('icon')->getClientOriginalExtension()); // obtencion extencion del archivo
            $name = Str::slug(str_replace($fileExt, '', $request->file('icon')->getClientOriginalName())); // nombre limpio del archivo
            $fileName = time() . '-' . $name . '.' . $fileExt; // nombre de guardado del archivo
        }
        $category = new Category();
        $category->name = $request->name;
        $category->slug = Str::of($request->name)->slug('-');
        $category->description = $request->description;
        $category->path = $path;
        $category->icon = $fileName;
        $category->attribution = e($request->attribution);
        if ($category->save()) {
            if ($request->hasFile('icon')) {
                $file = $request->file('icon');
                $file->move(public_path() . '/uploaded/' . $path, $fileName);
            }
            return redirect('/admin/categories')->with('message', '¡Se ha guardado correctamente la categoría!')->with('typealert', 'success');
        } else {
            return redirect('/admin/categories/create')->with('message', 'No se ha podido guardar la categoria, contacte al administrador del sitio.')->with('typealert', 'danger');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
